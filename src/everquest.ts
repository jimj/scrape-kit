import { Page } from "puppeteer";
import { Scraper } from "./scrape";

const REJECT_COOKIES_BUTTON = "#onetrust-reject-all-handler";
const SEARCH_INPUT_SELECTOR = "#header > form[name='search'] > input[name='q']";
const SEARCH_RESULT_SELECTOR = "#livesearch > ul > li:nth-child(1) > a";
const CLOSE_ADS_BUTTON = "#zaf-close-btn";

export class Allakhazam implements Scraper {
    readonly name: string;
    readonly url: string;

    constructor() {
        this.name = "eqalla";
        this.url = "https://everquest.allakhazam.com";
    }

    public async init(page: Page): Promise<void> {
        await this.closeAds(page);
        // Waiting for the 'Reject Cookies' button isn't quite good enough, sometimes throws Error.
        // I think this is due to the CSS animation & puppeteer's behavior to scroll an element into view.  
        // The Timeout is to let the animation fully load before looking for the button.
        await page.waitForTimeout(500)
        await page.waitForSelector(REJECT_COOKIES_BUTTON, { timeout: 10000 })
        await page.click(REJECT_COOKIES_BUTTON)
    }

    public async scrape(page: Page, item: string): Promise<any> {
        await this.closeAds(page);
        await page.type(SEARCH_INPUT_SELECTOR, item);
        try {
            await page.waitForSelector(SEARCH_RESULT_SELECTOR, { visible: true, timeout: 10000 });
        } catch (e: unknown) {
            return new Promise((_, reject) => reject());
        }

        await page.hover(SEARCH_RESULT_SELECTOR);
        await page.waitForTimeout(250); // ZAM lazy-loads image on hover

        await page.waitForSelector("#headtt > img", { visible: true });
        const tooltip = await page.$("#headtt");

        const iconData = await tooltip?.$("img").then(el => el?.screenshot({ encoding: 'base64' }));
        const descriptionData = await tooltip?.screenshot({ encoding: 'base64' });

        return {
            icon: iconData,
            description: descriptionData
        }
    }

    private async closeAds(page: Page) {
        try {
            await page.waitForSelector(CLOSE_ADS_BUTTON, { timeout: 2000 });
            await page.click(CLOSE_ADS_BUTTON);
            await page.evaluate(() => requestAnimationFrame(() => { }));
        } catch (err) {
            console.log(`Closing Ads did not work: ${err}`);
        }
    }
}