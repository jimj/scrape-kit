import { Request, Response } from "express";
import { Browser, Page } from "puppeteer"

export interface Scraper {
    name: string,
    url: string,
    init(page: Page): Promise<void>
    scrape(page: Page, query: string): Promise<any>
}

export class ScrapeKit {
    private readonly browser: Browser;
    private readonly scrapers: Scraper[] = []

    constructor(browser: Browser) {
        this.browser = browser;
    }

    public async register(scraper: Scraper) {
        process.stdout.write(`Registering scraper ${scraper.name}... `);
        const page = await this.initPage(scraper.url);

        try {
            await scraper.init(page);
            console.log("done")
        } catch (err) {
            console.log("failed");
            await this.capturePage(page);
        } finally {
            this.scrapers.push(scraper);
            page.close();
        }
    }

    public scrape(): (req: Request, res: Response, next: any) => void {
        return async (req: Request, res: Response, next: any) => {
            if (req.method === 'GET') {
                for (let scraper of this.scrapers) {
                    if (`/${scraper.name}` === req.path) {
                        if (!req.query.item) {
                            res.status(400).send("No 'item' query parameter");
                            return;
                        }

                        console.log(`Scraping ${scraper.name} for ${req.query.item}`);

                        const page = await this.initPage(scraper.url);

                        try {
                            const content = await scraper.scrape(page, `${req.query.item}`);
                            res.status(200).send(content);
                        } catch (err) {
                            console.log(`Scraping ${req.query.item} from ${scraper.name} failed: ${err}`);
                            await this.capturePage(page);
                        } finally {
                            page.close();
                        }
                    }
                }
            }

            next();
        }
    }

    private async initPage(url: string): Promise<Page> {
        const page = await this.browser.newPage();

        await page.goto(url);
        await page.evaluate(() => requestAnimationFrame(() => { }));
        await page.bringToFront();

        return page;
    }

    private async capturePage(page: Page): Promise<void> {
        const pageState = await page.screenshot({ encoding: 'base64' });
        console.log(`Page State: ${pageState}`);
    }
}
