import { Request, Response } from "express";
import { open } from "fs/promises";
import md5 from "md5";

const CACHE_DIR = process.env.SCRAPEKIT_CACHE || "./cache";

export async function cache(req: Request, res: Response, next: any): Promise<void> {
    const cacheKey = req.url.toLowerCase();
    const fileName = md5(cacheKey);
    const cacheFile = `${CACHE_DIR}/${fileName}`

    lookupCachedValue(cacheFile)
        .then(data => res.send(data))
        .catch(err => {
            cacheResponse(cacheFile, res);
            next();
        })
}

// Cache miss indicated by open failing & throwing if the file does not exist.
// Read failures treated as a cache miss.
async function lookupCachedValue(cacheFile: string): Promise<Buffer> {
    const fd = await open(cacheFile, 'r');

    try {
        const cachedData = await fd.readFile();
        return cachedData;
    } finally {
        fd.close();
    }
}

async function cacheResponse(cacheFile: string, res: Response): Promise<void> {
    const originalSend = res.send;

    res.send = function (data) {
        open(cacheFile, 'wx')
            .then(fd => fd.writeFile(JSON.stringify(data)).then(_ => fd.close()));

        res.send = originalSend;
        return res.send(data);
    }
}