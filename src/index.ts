import express, { Application } from "express";
import puppeteer from 'puppeteer';
import { cache } from "./cache";
import { Allakhazam } from "./everquest";
import { ScrapeKit } from "./scrape";

const app: Application = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(cache);

const launchArgs: puppeteer.BrowserLaunchArgumentOptions = process.env.NODE_ENV === "production"
    ? {
        args: [
            "--no-sandbox",
            "--disable-setuid-sandbox"
        ]
    }
    : { headless: false }

try {
    puppeteer.launch({
        ...launchArgs
    }).then(browser => {
        let scrapeKit = new ScrapeKit(browser);
        scrapeKit.register(new Allakhazam());
        app.use(scrapeKit.scrape());
    });

    app.listen(3000, (): void => {
        console.log(`Listening on 3000`);
    });
} catch (error: any) {
    console.error(`Error occured: ${error.message}`)
}