FROM node:16-buster-slim as nodebase

RUN  apt-get update \
    && apt-get install -y wget gnupg ca-certificates procps libxss1 \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    # Install Chrome to get all the OS level dependencies.  Puppeteer still uses its own packaged copy.
    && apt-get install -y google-chrome-stable \
    && rm -rf /var/lib/apt/lists/*

FROM nodebase as build
COPY package.json package-lock.json /
RUN npm install
COPY tsconfig.json /
COPY src src/
RUN npm run build

FROM nodebase as release
RUN useradd -d /scrapekit -m scrapekit
USER scrapekit
WORKDIR /scrapekit

COPY --chown=scrapekit package.json package-lock.json ./
ENV NODE_ENV=production
RUN npm install
COPY --from=build --chown=scrapekit dist dist/

CMD ["node", "."]